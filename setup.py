# -*- coding: utf-8 -*-

from setuptools import setup

setup(
    name='sepa',
    version='0.0.3',
    url='',
    author="ASK'Interactive",
    author_email='aabraham@askin.fr',
    packages=['sepa'],
    install_requires=['libComXML'],
    license='GPLv3',
    description='Minimal SEPA document (ISO 20022) generation library',
    long_description=open('README.md').read(),
)
