class SepaException(ValueError):
    """ General exception on SEPA document validation. """
    pass
