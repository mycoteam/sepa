""" Cet exemple reprend les éléments courants pour l’émission de SCT avec un BIC
donneur d’ordre non renseigné. Message émis par ABCD TELECOM SA constitué d’un
seul lot comprenant 2 ordres de virement SEPA.

Lot
- à débiter le 28/01/2011,
- sur le compte de ABCD TELECOM SA,
- dont l’IBAN est FR7630003012340001122334489
- portant la référence Remise 20110128 No 004578

Ordre de virement 1
- à créditer sur le compte de BAC IMMOBILIER,
- dont l’IBAN est FR7612345123450123456789006
- d’un montant de 51 252,98 EUR,
- portant la référence de bout en bout FAC20110100348,
- et le motif du paiement « loyer janvier 2011 »

Ordre de virement 2
- à créditer sur le compte de CABINET ARTHUR ET ASSOCIES,
- dont l’IBAN est FR7654321012340001234567816
- d’un montant de 2 500,00 EUR,
- portant la référence de bout en bout 586478954X,
- et le motif du paiement « acompte affaire DUCLOS »
"""

from datetime import date, datetime

import pytest

from sepa.exceptions import SepaException
from sepa.sepa19 import (
    AccountIdentification,
    AgentIdentifier,
    Amount,
    BankAccount,
    BankAgent,
    Concept,
    GenericPhysicalLegalEntity,
    OtherLegalEntity,
    PaymentIdentification,
    PaymentTypeInformation,
    PhysicalLegalEntity,
    SepaHeader,
    ServiceLevel,
)
from sepa.sepa34 import (
    CreditTransferTransactionInformation,
    CustomerCreditTransfer,
    CustomerCreditTransferDocument,
    PaymentInformation,
)


EXPECTED = """\
<Document xmlns="urn:iso:std:iso:20022:tech:xsd:pain.001.001.03">
  <CstmrCdtTrfInitn>
    <GrpHdr>
      <MsgId>MESS2011-65897</MsgId>
      <CreDtTm>2011-01-28T10:08:28</CreDtTm>
      <NbOfTxs>2</NbOfTxs>
      <CtrlSum>53752.98</CtrlSum>
      <InitgPty>
        <Nm>ABCD TELECOM SA</Nm>
      </InitgPty>
    </GrpHdr>
    <PmtInf>
      <PmtInfId>Remise 20110128 No 004578</PmtInfId>
      <PmtMtd>TRF</PmtMtd>
      <NbOfTxs>2</NbOfTxs>
      <CtrlSum>53752.98</CtrlSum>
      <PmtTpInf>
        <SvcLvl>
          <Cd>SEPA</Cd>
        </SvcLvl>
      </PmtTpInf>
      <ReqdExctnDt>2011-01-28</ReqdExctnDt>
      <Dbtr>
        <Nm>ABCD TELECOM SA</Nm>
      </Dbtr>
      <DbtrAcct>
        <Id>
          <IBAN>FR7630003012340001122334489</IBAN>
        </Id>
      </DbtrAcct>
      <DbtrAgt>
        <FinInstnId>
{debtor_agent_fininstnid}
        </FinInstnId>
      </DbtrAgt>
      <CdtTrfTxInf>
        <PmtId>
          <EndToEndId>FAC20110100348</EndToEndId>
        </PmtId>
        <Amt>
          <InstdAmt Ccy="EUR">51252.98</InstdAmt>
        </Amt>
        <Cdtr>
          <Nm>BAC IMMOBILIER</Nm>
        </Cdtr>
        <CdtrAcct>
          <Id>
            <IBAN>FR7612345123450123456789006</IBAN>
          </Id>
        </CdtrAcct>
        <RmtInf>
          <Ustrd>loyer janvier 2011</Ustrd>
        </RmtInf>
      </CdtTrfTxInf>
      <CdtTrfTxInf>
        <PmtId>
          <EndToEndId>586478954X</EndToEndId>
        </PmtId>
        <Amt>
          <InstdAmt Ccy="EUR">2500</InstdAmt>
        </Amt>
        <Cdtr>
          <Nm>CABINET ARTHUR ET ASSOCIES</Nm>
        </Cdtr>
        <CdtrAcct>
          <Id>
            <IBAN>FR7654321012340001234567816</IBAN>
          </Id>
        </CdtrAcct>
        <RmtInf>
          <Ustrd>acompte affaire DUCLOS</Ustrd>
        </RmtInf>
      </CdtTrfTxInf>
    </PmtInf>
  </CstmrCdtTrfInitn>
</Document>
"""

EXPECTED_NO_BIC = """\
          <Othr>
            <Id>NOTPROVIDED</Id>
          </Othr>"""

EXPECTED_BIC = """\
          <BIC>DEUTDEFF</BIC>"""


def get_test_credit_transfer_sepa_header():
    initiating_party = GenericPhysicalLegalEntity('InitgPty')
    initiating_party.feed({'entity_name': 'ABCD TELECOM SA'})
    sepa_header = SepaHeader()
    sepa_header.feed({
        'message_identification': 'MESS2011-65897',
        'creation_date_time': datetime(2011, 1, 28, 10, 8, 28).isoformat(),
        'number_of_transactions': '2',
        'control_sum': '53752.98',
        'initiating_party': initiating_party
    })
    return sepa_header


def get_test_credit_transfer_transaction_1():
    payment_identification = PaymentIdentification()
    payment_identification.feed({'end_to_end_identification': 'FAC20110100348'})
    amount = Amount()
    amount.feed({'instructed_amount': '51252.98'})
    amount.set_currency('EUR')
    creditor = GenericPhysicalLegalEntity('Cdtr')
    creditor.feed({'entity_name': 'BAC IMMOBILIER'})
    creditor_account_id = AccountIdentification()
    creditor_account_id.feed({'iban': 'FR7612345123450123456789006'})
    creditor_account = BankAccount('CdtrAcct')
    creditor_account.feed({'account_identification': creditor_account_id})
    remittance_information = Concept('RmtInf')
    remittance_information.feed({'unstructured': 'loyer janvier 2011'})
    credit_transfer_info = CreditTransferTransactionInformation()
    credit_transfer_info.feed({
        'payment_identification': payment_identification,
        'amount': amount,
        'creditor': creditor,
        'creditor_account': creditor_account,
        'remittance_information': remittance_information,
    })
    return credit_transfer_info


def get_test_credit_transfer_transaction_2():
    payment_identification = PaymentIdentification()
    payment_identification.feed({'end_to_end_identification': '586478954X'})
    amount = Amount()
    amount.feed({'instructed_amount': '2500'})
    amount.set_currency('EUR')
    creditor = GenericPhysicalLegalEntity('Cdtr')
    creditor.feed({'entity_name': 'CABINET ARTHUR ET ASSOCIES'})
    creditor_account_id = AccountIdentification()
    creditor_account_id.feed({'iban': 'FR7654321012340001234567816'})
    creditor_account = BankAccount('CdtrAcct')
    creditor_account.feed({'account_identification': creditor_account_id})
    remittance_information = Concept('RmtInf')
    remittance_information.feed({'unstructured': 'acompte affaire DUCLOS'})
    credit_transfer_info = CreditTransferTransactionInformation()
    credit_transfer_info.feed({
        'payment_identification': payment_identification,
        'amount': amount,
        'creditor': creditor,
        'creditor_account': creditor_account,
        'remittance_information': remittance_information,
    })
    return credit_transfer_info


def test_credit_transfer_without_bic():
    """ Test credit transfer document generation. """
    credit_transfer_info_1 = get_test_credit_transfer_transaction_1()
    credit_transfer_info_2 = get_test_credit_transfer_transaction_2()

    service_level = ServiceLevel()
    service_level.feed({'code': 'SEPA'})
    payment_type_info = PaymentTypeInformation()
    payment_type_info.feed({'service_level': service_level})
    debtor = GenericPhysicalLegalEntity('Dbtr')
    debtor.feed({'entity_name': 'ABCD TELECOM SA'})
    debtor_account_id = AccountIdentification()
    debtor_account_id.feed({'iban': 'FR7630003012340001122334489'})
    debtor_account = BankAccount('DbtrAcct')
    debtor_account.feed({'account_identification': debtor_account_id})
    debtor_agent_other_id = OtherLegalEntity()
    debtor_agent_other_id.feed({'identification': 'NOTPROVIDED'})
    financial_institution_identification = PhysicalLegalEntity('FinInstnId')
    financial_institution_identification.feed({'other': debtor_agent_other_id})
    debtor_agent = BankAgent('DbtrAgt')
    debtor_agent.feed({
        'financial_institution_identification': financial_institution_identification
    })
    payment_information = PaymentInformation()
    payment_information.feed({
        'payment_information_identification': 'Remise 20110128 No 004578',
        'payment_method': 'TRF',
        'number_of_transactions': '2',
        'control_sum': '53752.98',
        'payment_type_information': payment_type_info,
        'requested_execution_date': date(2011, 1, 28).isoformat(),
        'debtor': debtor,
        'debtor_account': debtor_account,
        'debtor_agent': debtor_agent,
        'credit_transfer_transaction_informations': [
            credit_transfer_info_1,
            credit_transfer_info_2
        ]
    })

    sepa_header = get_test_credit_transfer_sepa_header()
    customer_credit_transfer = CustomerCreditTransfer()
    customer_credit_transfer.feed({
        'sepa_header': sepa_header,
        'payment_informations': [
            payment_information
        ]
    })
    customer_credit_transfer_document = CustomerCreditTransferDocument()
    customer_credit_transfer_document.feed({'customer_credit_transfer': customer_credit_transfer})

    customer_credit_transfer_document.pretty_print = True
    customer_credit_transfer_document.build_tree()
    xml = str(customer_credit_transfer_document)
    assert xml == EXPECTED.format(debtor_agent_fininstnid=EXPECTED_NO_BIC)


def test_credit_transfer_with_bic():
    """ Test credit transfer document generation. """
    credit_transfer_info_1 = get_test_credit_transfer_transaction_1()
    credit_transfer_info_2 = get_test_credit_transfer_transaction_2()

    service_level = ServiceLevel()
    service_level.feed({'code': 'SEPA'})
    payment_type_info = PaymentTypeInformation()
    payment_type_info.feed({'service_level': service_level})
    debtor = GenericPhysicalLegalEntity('Dbtr')
    debtor.feed({'entity_name': 'ABCD TELECOM SA'})
    debtor_account_id = AccountIdentification()
    debtor_account_id.feed({'iban': 'FR7630003012340001122334489'})
    debtor_account = BankAccount('DbtrAcct')
    debtor_account.feed({'account_identification': debtor_account_id})
    financial_institution_identification = PhysicalLegalEntity('FinInstnId')
    financial_institution_identification.feed({'bic': 'DEUTDEFF'})
    debtor_agent = BankAgent('DbtrAgt')
    debtor_agent.feed({
        'financial_institution_identification': financial_institution_identification
    })
    payment_information = PaymentInformation()
    payment_information.feed({
        'payment_information_identification': 'Remise 20110128 No 004578',
        'payment_method': 'TRF',
        'number_of_transactions': '2',
        'control_sum': '53752.98',
        'payment_type_information': payment_type_info,
        'requested_execution_date': date(2011, 1, 28).isoformat(),
        'debtor': debtor,
        'debtor_account': debtor_account,
        'debtor_agent': debtor_agent,
        'credit_transfer_transaction_informations': [
            credit_transfer_info_1,
            credit_transfer_info_2
        ]
    })

    sepa_header = get_test_credit_transfer_sepa_header()
    customer_credit_transfer = CustomerCreditTransfer()
    customer_credit_transfer.feed({
        'sepa_header': sepa_header,
        'payment_informations': [
            payment_information
        ]
    })
    customer_credit_transfer_document = CustomerCreditTransferDocument()
    customer_credit_transfer_document.feed({'customer_credit_transfer': customer_credit_transfer})

    customer_credit_transfer_document.pretty_print = True
    customer_credit_transfer_document.build_tree()
    xml = str(customer_credit_transfer_document)
    assert xml == EXPECTED.format(debtor_agent_fininstnid=EXPECTED_BIC)


def test_invalid_sepa_field_values():
    debtor = GenericPhysicalLegalEntity('Dbtr')
    with pytest.raises(SepaException):
        debtor.feed({'entity_name': 'TURBOCA$H Corp'})
    with pytest.raises(SepaException):
        debtor.feed({'entity_name': '/SLASH INC'})
    with pytest.raises(SepaException):
        debtor.feed({'entity_name': 'SLASH INC/'})
    with pytest.raises(SepaException):
        debtor.feed({'entity_name': 'SLASH//INC'})
