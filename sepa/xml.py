import string

from libcomxml.core import XmlModel

from sepa.exceptions import SepaException


class SepaXmlModel(XmlModel):

    AUTHORIZED_CHARS = string.ascii_letters + string.digits + " /-?:().,'+"

    def feed(self, values):
        for key, value in values.items():
            if isinstance(value, str):
                self.validate_sepa_value(key, value)
        super().feed(values)

    @staticmethod
    def validate_sepa_value(field, value):
        """ Raise a SepaException on  """
        for char in value:
            if char not in SepaXmlModel.AUTHORIZED_CHARS:
                raise SepaException("Invalid char '{char}' in field '{field}': '{value}'".format(
                    char=char, field=field, value=value
                ))
        if value.startswith('/') or value.endswith('/'):
            raise SepaException("A field can't start or end with '/'")
        if '//' in value:
            raise SepaException("A field can't contain double slash '//'")
