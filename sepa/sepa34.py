# -*- coding: utf-8 -*-
""" More info and examples at http://www.iso20022.org/message_archive.page """

from libcomxml.core import XmlField

from sepa.sepa19 import (
    Amount,
    BankAccount,
    BankAgent,
    Concept,
    GenericPhysicalLegalEntity,
    PaymentIdentification,
    PaymentTypeInformation,
    Purpose,
    RegulatoryInformation,
    SepaHeader,
)
from sepa.xml import SepaXmlModel


############################### Level 2 ######################################


class CreditTransferTransactionInformation(SepaXmlModel):
    _sort_order = ('main_tag', 'payment_identification',
                   'payment_type_information', 'amount', 'charge_bearer',
                   'ultimate_debtor', 'creditor_agent', 'creditor',
                   'creditor_account', 'ultimate_creditor', 'purpose',
                   'regulatory_information', 'remittance_information')

    def __init__(self):
        self.main_tag = XmlField('CdtTrfTxInf')
        self.payment_identification = PaymentIdentification()
        self.payment_type_information = PaymentTypeInformation()
        self.amount = Amount()
        self.charge_bearer = XmlField('ChrgBr')
        self.ultimate_debtor = GenericPhysicalLegalEntity('UltmDbtr')
        self.creditor_agent = BankAgent('CdtrAgt')
        self.creditor = GenericPhysicalLegalEntity('Cdtr')
        self.creditor_account = BankAccount('CdtrAcct')
        self.ultimate_creditor = GenericPhysicalLegalEntity('UltmCdtr')
        self.purpose = Purpose()
        self.regulatory_information = RegulatoryInformation()
        self.remittance_information = Concept('RmtInf')
        super(CreditTransferTransactionInformation, self).__init__('CdtTrfTxInf', 'main_tag')


############################### Level 1 ######################################


class PaymentInformation(SepaXmlModel):
    _sort_order = ('main_tag', 'payment_information_identification', 'payment_method',
                   'batch_booking', 'number_of_transactions', 'control_sum',
                   'payment_type_information',
                   'requested_execution_date',
                   'debtor', 'debtor_account', 'debtor_agent',
                   'ultimate_debtor', 'charge_bearer',
                   'credit_transfer_transaction_informations')
    
    def __init__(self):
        self.main_tag = XmlField('PmtInf')
        self.payment_information_identification = XmlField('PmtInfId')
        self.payment_method = XmlField('PmtMtd')
        self.batch_booking = XmlField('BtchBookg')
        self.number_of_transactions = XmlField('NbOfTxs')
        self.control_sum = XmlField('CtrlSum')
        self.payment_type_information = PaymentTypeInformation()
        self.requested_execution_date = XmlField('ReqdExctnDt')
        self.debtor = GenericPhysicalLegalEntity('Dbtr')
        self.debtor_account = BankAccount('DbtrAcct')
        self.debtor_agent = GenericPhysicalLegalEntity('DbtrAgt')
        self.ultimate_debtor = GenericPhysicalLegalEntity('UltmtDbtr')
        self.charge_bearer = XmlField('ChrgBr')
        self.credit_transfer_transaction_informations = []  # CreditTransferTransactionInformation
        super(PaymentInformation, self).__init__('PmtInf', 'main_tag')


############################### Level 0 ######################################


class CustomerCreditTransfer(SepaXmlModel):
    _sort_order = ('main_tag', 'sepa_header',
                   'payment_informations')

    def __init__(self):
        self.main_tag = XmlField('CstmrCdtTrfInitn')
        self.sepa_header = SepaHeader()
        self.payment_informations = []  # PaymentInformation
        super(CustomerCreditTransfer, self).__init__('CstmrCdtTrfInitn', 'main_tag')


############################### Level -1 #####################################


class CustomerCreditTransferDocument(SepaXmlModel):
    _sort_order = ('root', 'customer_credit_transfer')

    def __init__(self):
        xmlns = "urn:iso:std:iso:20022:tech:xsd:pain.001.001.03"
        xsi = "http://www.w3.org/2001/XMLSchema-instance"
        
        self.root = XmlField('Document', attributes={'xmlns': xmlns})
        self.customer_credit_transfer = CustomerCreditTransfer()
        super(CustomerCreditTransferDocument, self).__init__('Document', 'root')
